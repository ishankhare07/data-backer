package backends

import (
	"context"
	"time"

	"cloud.google.com/go/storage"
)

type GCSClient struct {
	client    *storage.Client
	projectID string
}

func (g *GCSClient) WriteToFile(ctx context.Context, bucketName string, fileName string, content []byte) error {
	bucketHandle := g.client.Bucket(bucketName)

	objHandle := bucketHandle.Object("data-backer/" + fileName)
	objWriter := objHandle.NewWriter(ctx)

	defer objWriter.Close()

	_, err := objWriter.Write(content)
	return err
}

func (g *GCSClient) CreateBucket(ctx context.Context, name string) error {
	bucket := g.client.Bucket(name)
	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()

	return bucket.Create(ctx, g.projectID, nil)
}

func NewGCSClientOrDie(projectID string) Interface {
	c, err := storage.NewClient(context.Background())
	if err != nil {
		panic(err)
	}

	return &GCSClient{
		client:    c,
		projectID: projectID,
	}
}

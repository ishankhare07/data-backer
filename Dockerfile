FROM golang:1.15-buster

RUN echo "deb http://apt.postgresql.org/pub/repos/apt buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt update
RUN apt install -y default-mysql-client postgresql-client-14

COPY . /go/src/gitlab.com/ishankhare07/data-backer
RUN cd /go/src/gitlab.com/ishankhare07/data-backer && go build -o /usr/bin/data-backer /go/src/gitlab.com/ishankhare07/data-backer/main.go 

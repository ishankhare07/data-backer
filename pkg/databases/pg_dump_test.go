package databases

import (
	"testing"
)

func TestPostgresDump(t *testing.T) {
	pgcl := NewPGClient("localhost", "ahins", "example")

	output, err := pgcl.TestConnection()
	if err != nil {
		t.Fatalf("cannot test connection: %s", err.Error())
	}

	t.Log(string(output))

}

package databases

import "fmt"

const (
	Postgres = "postgres"
	Mysql    = "mysql"
)

type Interface interface {
	GetDump(db string) ([]byte, error)
	TestConnection() ([]byte, error)
}

type DriverInitializer func(host, username, password string) Interface

func GetDriver(dbDriver string) (DriverInitializer, error) {
	switch dbDriver {
	case Postgres:
		return NewPGClient, nil
	default:
		return nil, fmt.Errorf("unrecognized driver\n")
	}
}

package util

import (
	"time"
)

// This is the convention for filenames to be
// auto-generated for backups. We would want to use the
// timestamp with database name as a starter

func GetUniqueNameForDatabase(dbName string) string {
	ts := time.Now().Format(time.RFC3339)
	return dbName + "-" + ts
}

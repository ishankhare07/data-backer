module gitlab.com/ishankhare07/data-backer

go 1.15

require (
	cloud.google.com/go/storage v1.13.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
)

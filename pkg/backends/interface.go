package backends

import (
	"context"
	"fmt"
)

const (
	GCS = "gcs"
	S3  = "s3"
)

type Interface interface {
	CreateBucket(ctx context.Context, name string) error
	WriteToFile(ctx context.Context, bucketName string, fileName string, content []byte) error
}

type backendInitializer func(projectID string) Interface

func GetBackend(backend string) (backendInitializer, error) {
	switch backend {
	case GCS:
		return NewGCSClientOrDie, nil
	default:
		return nil, fmt.Errorf("unrecognized backend")
	}
}

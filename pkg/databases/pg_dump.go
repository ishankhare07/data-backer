package databases

import (
	"fmt"
	"os"
	"os/exec"
)

type postgres struct {
	host     string
	username string
	password string
}

func (p *postgres) TestConnection() ([]byte, error) {
	cmd := exec.Command("pg_isready", "-h", p.host, "-U", p.username)
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("PGPASSWORD=%s", p.password))

	output, err := cmd.Output()
	if err != nil {
		return []byte{}, err
	}

	return output, nil
}

func (p *postgres) GetDump(db string) ([]byte, error) {
	cmd := exec.Command("pg_dump", "-h", p.host, "-U", p.username, "-d", db)
	cmd.Env = append(os.Environ(),
		fmt.Sprintf("PGPASSWORD=%s", p.password))

	return cmd.CombinedOutput()
}

func NewPGClient(host, username, password string) Interface {
	return &postgres{
		host:     host,
		username: username,
		password: password,
	}
}

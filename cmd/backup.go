/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/ishankhare07/data-backer/pkg/backends"
	"gitlab.com/ishankhare07/data-backer/pkg/databases"
	"gitlab.com/ishankhare07/data-backer/pkg/util"
	"golang.org/x/net/context"
)

func exitIfError(err error) {
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
}

// backupCmd represents the backup command
var backupCmd = &cobra.Command{
	Use:   "backup",
	Short: "backup database dumps",
	Long:  `backup postgres or mysql dump to google cloud storage or aws s3 buckets`,
	Run: func(cmd *cobra.Command, args []string) {
		dbType, err := cmd.Flags().GetString("type")
		exitIfError(err)
		host, err := cmd.Flags().GetString("host")
		exitIfError(err)
		username, err := cmd.Flags().GetString("username")
		exitIfError(err)
		db, err := cmd.Flags().GetString("database")
		exitIfError(err)
		backend, err := cmd.Flags().GetString("backend")
		exitIfError(err)
		projectID, err := cmd.Flags().GetString("project")
		exitIfError(err)
		bucket, err := cmd.Flags().GetString("bucket")
		exitIfError(err)

		// ================================== start creating clients based on above objects =========================================
		driver, err := databases.GetDriver(dbType)
		exitIfError(err)

		dbClient := driver(host, username, "example") // fetch password from a secret
		dump, err := dbClient.GetDump(db)
		if err != nil {
			fmt.Println(string(dump))
			panic(err)
		}

		storageDriver, err := backends.GetBackend(backend)
		exitIfError(err)

		storageClient := storageDriver(projectID)

		err = storageClient.WriteToFile(context.Background(), bucket, util.GetUniqueNameForDatabase(db), dump) // bucketName
		exitIfError(err)
	},
}

func init() {
	rootCmd.AddCommand(backupCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// backupCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	backupCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	backupCmd.Flags().String("type", "postgres", "database type: postgres/mysql")
	backupCmd.Flags().String("host", "localhost", "host for database endpoint")
	backupCmd.Flags().StringP("username", "u", "", "username for the database")
	backupCmd.MarkFlagRequired("username")
	backupCmd.Flags().StringP("database", "d", "", "name of database to backup")
	backupCmd.MarkFlagRequired("database")
	backupCmd.Flags().String("backend", "gcs", "backend for storing backup files: gcs/s3")
	backupCmd.Flags().StringP("project", "p", "", "project ID for the storage bucket")
	backupCmd.MarkFlagRequired("project")
	backupCmd.Flags().StringP("bucket", "b", "", "bucket name for uploading the dump to")
	backupCmd.MarkFlagRequired("bucket")
}
